package br.uninove.cartolafc.model;

public class Status {
    private int rodadaAtual;
    private int statusMercado;
    private int esquemaDefaultId;
    private int cartoletaInicial;
    private int maxLigasFree;
    private int maxLigasPro;
    private int maxLigasMatamataFree;
    private int maxLigasMatamataPro;
    private int maxLigasPatrocinadasFree;
    private int maxLigasPatrocinadasProNum;
    private Boolean gameOver;
    private Long timesEscalados;
    private Boolean mercadoPosRodada;
    private String aviso;
    private Fechamento fechamento;

    public int getRodadaAtual() {
        return rodadaAtual;
    }

    public void setRodadaAtual(int rodadaAtual) {
        this.rodadaAtual = rodadaAtual;
    }

    public int getStatusMercado() {
        return statusMercado;
    }

    public void setStatusMercado(int statusMercado) {
        this.statusMercado = statusMercado;
    }

    public int getEsquemaDefaultId() {
        return esquemaDefaultId;
    }

    public void setEsquemaDefaultId(int esquemaDefaultId) {
        this.esquemaDefaultId = esquemaDefaultId;
    }

    public int getCartoletaInicial() {
        return cartoletaInicial;
    }

    public void setCartoletaInicial(int cartoletaInicial) {
        this.cartoletaInicial = cartoletaInicial;
    }

    public int getMaxLigasFree() {
        return maxLigasFree;
    }

    public void setMaxLigasFree(int maxLigasFree) {
        this.maxLigasFree = maxLigasFree;
    }

    public int getMaxLigasPro() {
        return maxLigasPro;
    }

    public void setMaxLigasPro(int maxLigasPro) {
        this.maxLigasPro = maxLigasPro;
    }

    public int getMaxLigasMatamataFree() {
        return maxLigasMatamataFree;
    }

    public void setMaxLigasMatamataFree(int maxLigasMatamataFree) {
        this.maxLigasMatamataFree = maxLigasMatamataFree;
    }

    public int getMaxLigasMatamataPro() {
        return maxLigasMatamataPro;
    }

    public void setMaxLigasMatamataPro(int maxLigasMatamataPro) {
        this.maxLigasMatamataPro = maxLigasMatamataPro;
    }

    public int getMaxLigasPatrocinadasFree() {
        return maxLigasPatrocinadasFree;
    }

    public void setMaxLigasPatrocinadasFree(int maxLigasPatrocinadasFree) {
        this.maxLigasPatrocinadasFree = maxLigasPatrocinadasFree;
    }

    public int getMaxLigasPatrocinadasProNum() {
        return maxLigasPatrocinadasProNum;
    }

    public void setMaxLigasPatrocinadasProNum(int maxLigasPatrocinadasProNum) {
        this.maxLigasPatrocinadasProNum = maxLigasPatrocinadasProNum;
    }

    public Boolean getGameOver() {
        return gameOver;
    }

    public void setGameOver(Boolean gameOver) {
        this.gameOver = gameOver;
    }

    public Long getTimesEscalados() {
        return timesEscalados;
    }

    public void setTimesEscalados(Long timesEscalados) {
        this.timesEscalados = timesEscalados;
    }

    public Boolean getMercadoPosRodada() {
        return mercadoPosRodada;
    }

    public void setMercadoPosRodada(Boolean mercadoPosRodada) {
        this.mercadoPosRodada = mercadoPosRodada;
    }

    public String getAviso() {
        return aviso;
    }

    public void setAviso(String aviso) {
        this.aviso = aviso;
    }

    public Fechamento getFechamento() {
        return fechamento;
    }

    public void setFechamento(Fechamento fechamento) {
        this.fechamento = fechamento;
    }

    @Override
    public String toString() {
        return "Status{" + "rodadaAtual=" + rodadaAtual + ", statusMercado=" + statusMercado + ", esquemaDefaultId=" + esquemaDefaultId + ", cartoletaInicial=" + cartoletaInicial + ", maxLigasFree=" + maxLigasFree + ", maxLigasPro=" + maxLigasPro + ", maxLigasMatamataFree=" + maxLigasMatamataFree + ", maxLigasMatamataPro=" + maxLigasMatamataPro + ", maxLigasPatrocinadasFree=" + maxLigasPatrocinadasFree + ", maxLigasPatrocinadasProNum=" + maxLigasPatrocinadasProNum + ", gameOver=" + gameOver + ", timesEscalados=" + timesEscalados + ", mercadoPosRodada=" + mercadoPosRodada + ", aviso=" + aviso + ", fechamento=" + fechamento + '}';
    }

    
    
}
