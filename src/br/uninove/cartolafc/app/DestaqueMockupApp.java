package br.uninove.cartolafc.app;

import br.uninove.cartolafc.mockup.DestaqueMockup;
import br.uninove.cartolafc.model.Destaque;
import java.util.List;

public class DestaqueMockupApp {
    public static void main(String[] args) {
        Destaque mockup = DestaqueMockup.get();
//        System.out.println(mockup);
//        System.out.println("\n ===================");
        
        List<Destaque> destaques;
        destaques = DestaqueMockup.getList();
//        System.out.println(destaques);
        
        for (int i = 0; i < destaques.size(); i++) {
            mockup = destaques.get(i);
            System.out.println(mockup);
        }
        
//        System.out.println(destaques.get(0));
//        System.out.println(destaques.get(1));
    }
    
}
