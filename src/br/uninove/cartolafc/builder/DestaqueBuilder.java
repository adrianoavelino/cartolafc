package br.uninove.cartolafc.builder;

import br.uninove.cartolafc.dto.DestaqueDto;
import br.uninove.cartolafc.model.Destaque;

public class DestaqueBuilder {
    public static Destaque build(DestaqueDto dto) {
        Destaque result = new Destaque();
        
        result.setAtleta(dto.getAtleta());
        result.setClube(dto.getClube());
        result.setEscalacoes(dto.getEscalacoes());
        result.setEscudoClube(dto.getEscudo_clube());
        result.setPosicao(dto.getPosicao());
        
        
        return result;
    }
    
}
