package br.uninove.cartolafc.model;

public class Escudo {
    private String _50x50;
    private String _45x45;
    private String _30x30;

    public String get50x50() {
        return _50x50;
    }

    public void set50x50(String _50x50) {
        this._50x50 = _50x50;
    }

    public String get45x45() {
        return _45x45;
    }

    public void set45x45(String _45x45) {
        this._45x45 = _45x45;
    }

    public String get30x30() {
        return _30x30;
    }

    public void set30x30(String _30x30) {
        this._30x30 = _30x30;
    }

    @Override
    public String toString() {
        return "Escudos{" + "_50x50=" + _50x50 + ", _45x45=" + _45x45 + ", _30x30=" + _30x30 + '}';
    }
    
    
}
