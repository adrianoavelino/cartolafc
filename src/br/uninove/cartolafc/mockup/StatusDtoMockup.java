package br.uninove.cartolafc.mockup;

import br.uninove.cartolafc.dto.StatusDto;
import br.uninove.cartolafc.model.Fechamento;
import br.uninove.cartolafc.model.Status;
import java.util.ArrayList;
import java.util.List;

public class StatusDtoMockup {
    public static List<StatusDto> getList() {
        
        Fechamento fechamento;
        
        fechamento =  new Fechamento();
        fechamento.setDia(2);
        fechamento.setMes(11);
        fechamento.setAno(2016);
        fechamento.setHora(9);
        fechamento.setMinuto(1);
        fechamento.setTimestamp(1475323200);
        
        List<StatusDto> result;
        result =  new ArrayList<>();
        
        
        StatusDto status;
        
        status = new StatusDto();
        status.setRodada_atual(29);
        status.setStatus_mercado(2);
        status.setEsquema_default_id(5);
        status.setCartoleta_inicial(101);
        status.setMax_ligas_free(3);
        status.setMax_ligas_pro(6);
        status.setMax_ligas_matamata_free(6);
        status.setMax_ligas_matamata_pro(6);
        status.setMax_ligas_patrocinadas_free(3);
        status.setMax_ligas_patrocinadas_pro_num(3);
        status.setGame_over(false);
        status.setTimes_escalados(1541322l);
        status.setFechamento(fechamento);
        status.setMercado_pos_rodada(true);
        status.setAviso("avisooooo");
        result.add(status);
        
        return  result;
    }    
    
}
