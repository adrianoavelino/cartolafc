package br.uninove.cartolafc.app;

import br.uninove.cartolafc.builder.StatusBuilder;
import br.uninove.cartolafc.dto.StatusDto;
import br.uninove.cartolafc.mockup.StatusDtoMockup;
import br.uninove.cartolafc.model.Status;
import java.util.List;

public class StatusBuilderApp {
    public static void main(String[] args) {
        List<StatusDto> result = StatusDtoMockup.getList();
        
        Status status;
        for (int i = 0; i < result.size(); i++) {
            StatusDto dto = result.get(i);
            System.out.println(StatusBuilder.build(dto));
            System.out.println(dto);
        }
        
    }
    
}
