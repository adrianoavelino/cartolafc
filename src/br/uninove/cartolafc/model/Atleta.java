package br.uninove.cartolafc.model;

public class Atleta {
    private int atletaId;
    private String nome;
    private String apelido;
    private String foto;
    private int precoEditorial;

    public int getAtletaId() {
        return atletaId;
    }

    public void setAtletaId(int atletaId) {
        this.atletaId = atletaId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getPrecoEditorial() {
        return precoEditorial;
    }

    public void setPrecoEditorial(int precoEditorial) {
        this.precoEditorial = precoEditorial;
    }

    @Override
    public String toString() {
        return "Atleta{" + "atletaId=" + atletaId + ", nome=" + nome + ", apelido=" + apelido + ", foto=" + foto + ", precoEditorial=" + precoEditorial + '}';
    }

    
    
    
}
