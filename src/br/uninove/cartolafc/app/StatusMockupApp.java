package br.uninove.cartolafc.app;

import br.uninove.cartolafc.mockup.StatusMockup;
import br.uninove.cartolafc.model.Status;
import java.util.List;

public class StatusMockupApp {
    public static void main(String[] args) {
        
        Status status = StatusMockup.get();
        System.out.println(status);
        
        
        List<Status> statusMockup = StatusMockup.getList();
        
   
        for (int i = 0; i < statusMockup.size(); i++) {
            status = statusMockup.get(i);
            System.out.println(status);
        }
        
    }
    
}
