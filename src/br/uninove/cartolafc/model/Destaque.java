package br.uninove.cartolafc.model;

public class Destaque {
    private Atleta atleta;
    private int escalacoes;
    private String clube;
    private String escudoClube;
    private String posicao;

    public Atleta getAtleta() {
        return atleta;
    }

    public void setAtleta(Atleta atleta) {
        this.atleta = atleta;
    }

    public int getEscalacoes() {
        return escalacoes;
    }

    public void setEscalacoes(int escalacoes) {
        this.escalacoes = escalacoes;
    }

    public String getClube() {
        return clube;
    }

    public void setClube(String clube) {
        this.clube = clube;
    }

    public String getEscudoClube() {
        return escudoClube;
    }

    public void setEscudoClube(String escudoClube) {
        this.escudoClube = escudoClube;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    @Override
    public String toString() {
        return "Destaque{" + "atleta=" + atleta + ", escalacoes=" + escalacoes + ", clube=" + clube + ", escudoClube=" + escudoClube + ", posicao=" + posicao + '}';
    }

    
    
}
