package br.uninove.cartolafc.mockup;

import br.uninove.cartolafc.model.Fechamento;
import br.uninove.cartolafc.model.Status;
import java.util.ArrayList;
import java.util.List;

public class StatusMockup {
    
    public static Status get() {

                
        Status status = new Status();
        
        Fechamento fechamento;
        fechamento =  new Fechamento();
        fechamento.setDia(2);
        fechamento.setMes(11);
        fechamento.setAno(2016);
        fechamento.setHora(9);
        fechamento.setMinuto(1);
        fechamento.setTimestamp(1475323200);
        
        status.setRodadaAtual(29);
        status.setStatusMercado(2);
        status.setEsquemaDefaultId(5);
        status.setCartoletaInicial(101);
        status.setMaxLigasFree(3);
        status.setMaxLigasPro(6);
        status.setMaxLigasMatamataFree(6);
        status.setMaxLigasMatamataPro(6);
        status.setMaxLigasPatrocinadasFree(3);
        status.setMaxLigasPatrocinadasProNum(3);
        status.setGameOver(false);
        status.setTimesEscalados(1541322l);
        status.setFechamento(fechamento);
        status.setMercadoPosRodada(true);
        status.setAviso("avisooooo");
        
        return  status;
    }
    
    public static List<Status> getList() {
        
        Fechamento fechamento;
        
        fechamento =  new Fechamento();
        fechamento.setDia(2);
        fechamento.setMes(11);
        fechamento.setAno(2016);
        fechamento.setHora(9);
        fechamento.setMinuto(1);
        fechamento.setTimestamp(1475323200);
        
        List<Status> result;
        result =  new ArrayList<>();
        
        
        Status status;
        
        status = new Status();
        status.setRodadaAtual(29);
        status.setStatusMercado(2);
        status.setEsquemaDefaultId(5);
        status.setCartoletaInicial(101);
        status.setMaxLigasFree(3);
        status.setMaxLigasPro(6);
        status.setMaxLigasMatamataFree(6);
        status.setMaxLigasMatamataPro(6);
        status.setMaxLigasPatrocinadasFree(3);
        status.setMaxLigasPatrocinadasProNum(3);
        status.setGameOver(false);
        status.setTimesEscalados(1541322l);
        status.setFechamento(fechamento);
        status.setMercadoPosRodada(true);
        status.setAviso("avisooooo");
        result.add(status);
        
        return  result;
    }
}
