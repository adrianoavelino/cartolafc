package br.uninove.cartolafc.mockup;

import br.uninove.cartolafc.model.Atleta;
import br.uninove.cartolafc.model.Destaque;
import java.util.ArrayList;
import java.util.List;

public class DestaqueMockup {
    public static Destaque  get() {
        Destaque result = new Destaque();
        
        Atleta atleta = new Atleta();
        atleta.setAtletaId(1);
        atleta.setNome("Joaquim Barbosa");
        atleta.setApelido("Miro");
        atleta.setFoto("http://caminhodafoto/0001.jpg");
        atleta.setPrecoEditorial(123);
        
        result.setAtleta(atleta);
        result.setClube("SPO");
        result.setEscalacoes(123);
        result.setEscudoClube("time.jpg");
        result.setPosicao("meia");
        
        return result;
    }
    
    public static List<Destaque> getList() {
        List<Destaque> result;
        result = new ArrayList<>();
        
        Destaque destaque;
        Atleta atleta;
        
        //destaque 1
        atleta = new Atleta();
        atleta.setAtletaId(1);
        atleta.setNome("Jośe Romário");
        atleta.setApelido("Romario");
        atleta.setFoto("romario.jpg");
        atleta.setPrecoEditorial(1200);
        
        destaque = new Destaque();
        destaque.setAtleta(atleta);
        destaque.setClube("FLA");
        destaque.setEscalacoes(123);
        destaque.setEscudoClube("escudoflamengo.jpg");
        destaque.setPosicao("meia");
        result.add(destaque);

        
        //destaque 2
        atleta = new Atleta();
        atleta.setAtletaId(2);
        atleta.setNome("Neymar junior");
        atleta.setApelido("Neymar");
        atleta.setFoto("neymar.jpg");
        atleta.setPrecoEditorial(21200);
        
        destaque = new Destaque();
        destaque.setAtleta(atleta);
        destaque.setClube("BAR");
        destaque.setEscalacoes(103);
        destaque.setEscudoClube("escudobarca.jpg");
        destaque.setPosicao("atacante");
        result.add(destaque);
        
        
        return result;
    }
}
