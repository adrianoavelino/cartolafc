package br.uninove.cartolafc.app;

import br.uninove.cartolafc.builder.DestaqueBuilder;
import br.uninove.cartolafc.dto.DestaqueDto;
import br.uninove.cartolafc.mockup.DestaqueDtoMockup;
import br.uninove.cartolafc.model.Destaque;
import java.util.List;

public class DestaqueBuilderApp {
    public static void main(String[] args) {
        List<DestaqueDto>  destaqueDto = DestaqueDtoMockup.getList();
        
        
        for (int i = 0; i < destaqueDto.size(); i++) {
            DestaqueDto dto = destaqueDto.get(i);
//            System.out.println(dto);
            
            Destaque destaque = DestaqueBuilder.build(dto);
            System.out.println(destaque);
            System.out.println("***************************");
        }
        
    }
    
}
