package br.uninove.cartolafc.app;

import br.uninove.cartolafc.mockup.ClubeMockup;
import br.uninove.cartolafc.model.Clube;
import java.util.List;

public class ClubeMockupApp {
    public static void main(String[] args) {
        Clube clube = ClubeMockup.get();
//        System.out.println(clube);

        List<Clube> result = ClubeMockup.getList();
        
        for (int i = 0; i < result.size(); i++) {
            clube = result.get(i);
            System.out.println(clube);
            
        }

    }
    
//    11 99759-9276
}
