package br.uninove.cartolafc.builder;

import br.uninove.cartolafc.dto.StatusDto;
import br.uninove.cartolafc.model.Status;

public class StatusBuilder {
    public static Status build (StatusDto dto) {
        
        Status result = new Status();
        
        result.setRodadaAtual(dto.getRodada_atual());
        result.setStatusMercado(dto.getStatus_mercado());
        result.setEsquemaDefaultId(dto.getEsquema_default_id());
        result.setCartoletaInicial(dto.getCartoleta_inicial());
        result.setMaxLigasFree(dto.getMax_ligas_free());
        result.setMaxLigasPro(dto.getMax_ligas_pro());
        result.setMaxLigasPatrocinadasFree(dto.getMax_ligas_patrocinadas_free());
        result.setMaxLigasPatrocinadasProNum(dto.getMax_ligas_patrocinadas_pro_num());
        result.setGameOver(dto.getGame_over());
        result.setTimesEscalados(dto.getTimes_escalados());
        result.setFechamento(dto.getFechamento());
        result.setMercadoPosRodada(dto.getMercado_pos_rodada());
        result.setAviso(dto.getAviso());
        
        return result;
    }
    
}
