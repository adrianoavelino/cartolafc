package br.uninove.cartolafc.dto;

public class AtletaDto {
    private int atleta_id;
    private String nome;
    private String apelido;
    private String foto;
    private int preco_editorial;

    public int getAtleta_id() {
        return atleta_id;
    }

    public void setAtleta_id(int atleta_id) {
        this.atleta_id = atleta_id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getPreco_editorial() {
        return preco_editorial;
    }

    public void setPreco_editorial(int preco_editorial) {
        this.preco_editorial = preco_editorial;
    }

    @Override
    public String toString() {
        return "Atleta{" + "atleta_id=" + atleta_id + ", nome=" + nome + ", apelido=" + apelido + ", foto=" + foto + ", preco_editorial=" + preco_editorial + '}';
    }

    
}
