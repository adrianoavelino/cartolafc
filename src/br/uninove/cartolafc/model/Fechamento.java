package br.uninove.cartolafc.model;

import java.security.Timestamp;

public class Fechamento {
    private int dia;
    private int mes;
    private int ano;
    private int hora;
    private int minuto;
    private long timestamp;

//timestamp    
//    System.out.println(Calendar.getInstance().getTimeInMillis());

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Fechamento{" + "dia=" + dia + ", mes=" + mes + ", ano=" + ano + ", hora=" + hora + ", minuto=" + minuto + ", timestamp=" + timestamp + '}';
    }
  
//    public static void main(String[] args) {
//        long teste = Calendar.getInstance().getTimeInMillis();
//        System.out.println("miliii " + teste);
//    }
    

    
}
