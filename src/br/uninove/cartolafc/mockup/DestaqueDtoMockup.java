package br.uninove.cartolafc.mockup;

import br.uninove.cartolafc.dto.DestaqueDto;
import br.uninove.cartolafc.model.Atleta;
import java.util.ArrayList;
import java.util.List;

public class DestaqueDtoMockup {
    public static List<DestaqueDto> getList() {
        List<DestaqueDto> result = new ArrayList<>();
        
        DestaqueDto dto;
        Atleta atleta;
        
        //destaque 1
        dto = new DestaqueDto();
        atleta = new Atleta();
        atleta.setAtletaId(100);
        atleta.setNome("David Luz");
        atleta.setApelido("David");
        atleta.setFoto("david.jpg");
        atleta.setPrecoEditorial(1000);
        
        
        dto.setAtleta(atleta);
        dto.setClube("Grêmio");
        dto.setEscalacoes(50);
        dto.setEscudo_clube("gre.jpg");
        dto.setPosicao("meia");
        result.add(dto);
        
        //destaque 2
        dto = new DestaqueDto();
        atleta = new Atleta();
        atleta.setAtletaId(38909);
        atleta.setNome("Diego Ribas da Cunha");
        atleta.setApelido("Diego");
        atleta.setFoto("Diego.jpg");
        atleta.setPrecoEditorial(10);
        
        dto.setAtleta(atleta);
        dto.setClube("PAL");
        dto.setEscalacoes(5469503);
        dto.setEscudo_clube("gre.jpg");
        dto.setPosicao("Zagueiro");
        result.add(dto);
        
        return result;
    }
}
