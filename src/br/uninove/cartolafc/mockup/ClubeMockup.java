package br.uninove.cartolafc.mockup;

import br.uninove.cartolafc.model.Clube;
import br.uninove.cartolafc.model.Escudo;
import java.util.ArrayList;
import java.util.List;

public class ClubeMockup {
    
    public static Clube get() {
        Clube result;
        Escudo escudo;

        //clube 1
        escudo = new Escudo();
        escudo.set30x30("fla30x30.jpg");
        escudo.set45x45("fla45x45.jpg");
        escudo.set50x50("fla50x50.jpg");
        
        result = new Clube();
        result.setId(262);
        result.setNome("Flamengo");
        result.setAbreviacao("FLA");
        result.setPosicao(2);
        result.setEscudos(escudo);
        
        return result;
    }
    
    public static List<Clube> getList() {
        List<Clube> result = new ArrayList<>();
        Escudo escudo;
        Clube clube;

        //clube 1
        escudo = new Escudo();
        escudo.set30x30("fla30x30.jpg");
        escudo.set45x45("fla45x45.jpg");
        escudo.set50x50("fla50x50.jpg");
        
        clube = new Clube();
        clube.setId(262);
        clube.setNome("Flamengo");
        clube.setAbreviacao("FLA");
        clube.setPosicao(2);
        clube.setEscudos(escudo);
        result.add(clube);

        //clube 2
        escudo = new Escudo();
        escudo.set30x30("bot30x30.jpg");
        escudo.set45x45("bot45x45.jpg");
        escudo.set50x50("bot50x50.jpg");
        
        clube = new Clube();
        clube.setId(263);
        clube.setNome("Botafogo");
        clube.setAbreviacao("BOT");
        clube.setPosicao(10);
        clube.setEscudos(escudo);
        result.add(clube);

        //clube 3
        escudo = new Escudo();
        escudo.set30x30("");
        escudo.set45x45("");
        escudo.set50x50("");
        
        clube = new Clube();
        clube.setId(265);
        clube.setNome("Bahia");
        clube.setAbreviacao("BAH");
//        clube.setPosicao(10);
        clube.setEscudos(escudo);
        result.add(clube);
        
        return result;
    }
    
}
